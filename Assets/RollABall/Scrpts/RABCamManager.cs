﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RABCamManager : MonoBehaviour {

	public GameObject playerRab;

	private Vector3 offset;

	void Start ()
	{
		offset = transform.position - playerRab.transform.position;
	}

	void LateUpdate ()
	{
		transform.position = playerRab.transform.position + offset;
	}
}
