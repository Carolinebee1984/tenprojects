using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObstaclesSpawnFB : MonoBehaviour {
	public int SpawnRateMax;
	public int SpawnRateMin;
	public int MinHeight;
	public int MaxHeight;
	public GameObject pipe;

	// Use this for initialization
	void Start () {
		StartCoroutine ("Spawner");
	}
	
	IEnumerator Spawner()
	{
		yield return new WaitForSeconds (Random.Range (SpawnRateMin, SpawnRateMax));
		Instantiate (pipe, new Vector3 (this.transform.position.x, Random.Range (MinHeight, MaxHeight), this.transform.position.z), Quaternion.identity);
		StartCoroutine ("Spawner");
	}
}
