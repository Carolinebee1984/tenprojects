﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PipesManager : MonoBehaviour {

	public float Speed;

	// Update is called once per frame
	public void Update () {
		this.transform.Translate (-Speed, 0, 0);
	}

	// Use this for initialization
	void Start () {
		StartCoroutine ("Destroy");
	}

	IEnumerator Destroy ()
	{
		yield return new WaitForSeconds (10);
		Destroy (this.gameObject);

	}

}
