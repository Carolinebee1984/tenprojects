﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI; 

public class FBPM : MonoBehaviour {
	
	public Rigidbody2D rbfb;
	public float speedfb;
	private bool flap;

	[SerializeField] Text countText;
	private int count;

	[SerializeField] GameObject gameoverFb;
	public Transform posJuegoOver;

	// Use this for initialization
	void Start () {
		rbfb = GetComponent <Rigidbody2D> ();
		count = 0;
		SetCountText ();

	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetMouseButtonDown (0))
		{
			flap = true;
			rbfb.velocity = new Vector2 (0, 0);
			StartCoroutine ("stopFlap");

		}
	}

	void FixedUpdate (){

		if (flap) 
		{
		   
			rbfb.AddForce (Vector2.up * speedfb);

		}
	}

	IEnumerator stopFlap ()
	{
		yield return new WaitForSeconds (0.1f);
		flap = false; 
	
	}

	void OnCollisionEnter2D (Collision2D other){
		Destroy (gameObject);
		Instantiate (gameoverFb, posJuegoOver.position, posJuegoOver.rotation);
		Time.timeScale = 0; 


	
	}


	void OnTriggerEnter2D(Collider2D other) {
		if (other.gameObject.CompareTag ("triggered")) {
			count = count + 1;
			SetCountText ();

		}
	
	}

	void SetCountText ()
	{
		countText.text = "Score: " + count.ToString ();
	}
}
