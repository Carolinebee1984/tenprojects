﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Bullet_Ctrl : MonoBehaviour {
	private Transform bullet;
	[SerializeField] float vel;



	// Use this for initialization
	void Start () {

		bullet = transform;

	}

	void Update (){
		//mover bala arriba 
		float amtToMove = vel * Time.deltaTime;
		bullet.Translate (Vector2.up * amtToMove);

	}

	void OnTriggerEnter2D (Collider2D otherObject)
	{
		Debug.Log ("We hit: " + otherObject.name);


		//si el objeto con el que colisiona es "enemigo"
		if (otherObject.tag == "Enemies") 
		{
			IncreaseScore ();
			Destroy (otherObject.gameObject);
			Destroy (gameObject);
		}
	
	}

	void OnBecameInvisible(){

		//destruir bala al salir de donde la camara puede ver
		Destroy (gameObject);
	}

	void IncreaseScore() 
	{
		var textUIComp = GameObject.Find ("Score_SI").GetComponent<Text> ();
		int scoresi = int.Parse (textUIComp.text);

		scoresi += 20;

		textUIComp.text = scoresi.ToString (); 
	}

	}
