﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyBullet_Ctrl : MonoBehaviour {
	private Transform bulletE;
	[SerializeField] float velE;




	// Use this for initialization
	void Start () {

		bulletE = transform;

	}

	void Update (){
		//mover bala arriba 
		float amt2Move = velE * Time.deltaTime;
		bulletE.Translate (Vector2.down * amt2Move);

	}

	void OnTriggerEnter2D (Collider2D otherObject)
	{
		Debug.Log ("We hit: " + otherObject.name);

		//si el objeto con el que colisiona es "enemigo"
		if (otherObject.tag == "Play") 
		{
			Destroy (otherObject.gameObject); //en lugar de destroy seria -1vida y -3 vidas: destroy y game over
			Time.timeScale = 0;
		}
		if (otherObject.tag == "Bases") {
			Destroy (otherObject.gameObject);
		}
	}

	void OnBecameInvisible(){

		//destruir bala al salir de donde la camara puede ver
		Destroy (gameObject);
	}

}



