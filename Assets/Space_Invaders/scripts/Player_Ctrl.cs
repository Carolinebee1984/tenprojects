﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player_Ctrl : MonoBehaviour {
	[SerializeField] float speed;

	[SerializeField] GameObject shootprefab;
	[SerializeField] bool recargaBola = true; //recargabola es verdadero cuando la bala va a ser lanzada
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
		if (Input.GetKey (KeyCode.LeftArrow) && transform.position.x > -6.5) {
			transform.Translate (Vector2.left * Time.deltaTime * speed);
		}

		if (Input.GetKey (KeyCode.RightArrow) && transform.position.x < 6.5) {
			transform.Translate (Vector2.right * Time.deltaTime * speed);
		}

		if(Input.GetKey (KeyCode.Space) && recargaBola){ //recargabola es para el cooldown
			//fire bullet
			recargaBola = false; //cuando se aprieta space, recargabola se convierte en falso, por lo que ya no van a salir balas
			Invoke("Cooldown", 0.8f); //y saldran balas de nuevoo despues de 0.8 segundos
			
		}


	}

	void Cooldown(){

		Instantiate (shootprefab, transform.position, Quaternion.identity);
		recargaBola = true; //la bala vuelve a ser true cuando la condicion de los 0.8 segundos se cumpla.
	}
}
