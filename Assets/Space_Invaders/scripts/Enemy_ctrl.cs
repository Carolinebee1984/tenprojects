﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy_ctrl : MonoBehaviour {

	[SerializeField] GameObject bulletEprefab; //prefab bala

	[SerializeField] float minimoRate = 1.0f;//parametros para
	[SerializeField] float maxRate = 4.0f;//random
	[SerializeField] float waitTime = 4.0f;//y tiempo de espera

	private float speed;//parametros para mover enemigos
	private Transform enemyHold;//parametros para mover enemigos 


	// Use this for initialization
	void Start () {
		waitTime = waitTime + Random.Range (minimoRate, maxRate); //waittime incluye el rango random entre los dos valores de min y max que pusimos arriba
		//enemyHold.GetComponent<Transform>();
	}


	void FixedUpdate () {
		if (Time.time > waitTime) { // si el tiempo es mayor que la variable de tiempo para lanzar las cositas random
			waitTime = waitTime + Random.Range (minimoRate, maxRate); //se van a lanzar randomente

			Instantiate (bulletEprefab, transform.position, transform.rotation);  //como instancias 
		}
	}


}
