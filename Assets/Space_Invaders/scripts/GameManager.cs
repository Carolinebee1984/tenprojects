﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
	[SerializeField] GameObject alien;

	public static GameManager GM;



	void Awake()
	{
		if (GM == null)
		{
			GM = this;
			DontDestroyOnLoad(this);
		}
		else
		{
			Destroy(gameObject);
		}
	}

	void Start ()
	{
		//aliens aparencen dentro del espacio entre los numeros float
		for (float x =-4.5f; x <= 4.5f; x++)
		{
			Instantiate(alien, new Vector3(x,0,0), Quaternion.identity);// * 1.2f

			for (float y = 1; y <= 4; y++)

				Instantiate (alien, new Vector3(x,y,0), Quaternion.identity);
		}

	}
}
