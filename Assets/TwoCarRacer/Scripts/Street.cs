﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Street : MonoBehaviour {

	Renderer rend;

	// Use this for initialization
	void Start () {
		rend = GetComponent <Renderer>();
	}
	
	// Update is called once per frame
	void Update () {

		float offset = Time.time * 0.5f;
		rend.material.SetTextureOffset ("_MainTex", new Vector2 (0, offset)); 
	}
}
