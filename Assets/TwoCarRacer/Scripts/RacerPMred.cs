﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RacerPMred : MonoBehaviour {

	private bool isPlayerMoving = false;
	[SerializeField] float speedRed;

	[SerializeField] Text scoreCounter;
	private int count;

	[SerializeField] Text loseText;





	void Start () {
		
		loseText.text = "";
	}

	// Update is called once per frame
	void Update () {
		if (Input.GetKeyDown (KeyCode.RightArrow) && isPlayerMoving == false) 
		{ 
			StartCoroutine (OneMoveTo (21f));
		}

		if (Input.GetKeyDown (KeyCode.LeftArrow) && isPlayerMoving == false) 
		{ 
			StartCoroutine (OneMoveTo (5.8f));
		}
	}

	IEnumerator OneMoveTo (float tarX)
	{
		isPlayerMoving = true;

		float distance = Vector2.Distance (transform.position, new Vector2 (tarX, 0));

		Vector2 tarPos = new Vector2 (tarX, transform.position.y);

		while (distance > 16.5f)
		{
			distance = Vector2.Distance (transform.position, new Vector2 (tarX, 0));

			if (transform.position.x > tarX)
			{ 
				transform.position = Vector2.MoveTowards (new Vector2 (transform.position.x, transform.position.y), tarPos, speedRed * Time.deltaTime);
			}


			else if (transform.position.x < tarX)
			{
				transform.position = Vector2.MoveTowards (new Vector2 (transform.position.x, transform.position.y), tarPos, speedRed * Time.deltaTime);
			}

			yield return new WaitForEndOfFrame ();
		}

		transform.position = new Vector3 (tarX, transform.position.y, transform.position.z);
		isPlayerMoving = false;

	}

	void OnTriggerEnter2D (Collider2D other){
		if (other.gameObject.CompareTag ("Red")) {
			Destroy (other.gameObject);
			TwoRacerGM.GM.Score ();
			//y actualiza contador
		
			} 
		else if (other.gameObject.CompareTag ("Green")) {
			Destroy (this.gameObject);
			//display game over
			loseText.text = "GAME OVER!";
			Time.timeScale = 0;
			//time.Timescale = 0;

		}
	
	}


}
