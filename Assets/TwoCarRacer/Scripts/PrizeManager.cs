﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PrizeManager : MonoBehaviour {

	private float speedTR = 0.2f ;
	private float multiplierTR = -0.01f;




	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
		this.transform.Translate (0, -speedTR -multiplierTR, 0);


	}

	void OnTriggerEnter2D (Collider2D other){
		if (other.gameObject.CompareTag ("PlayerTR")) {
			multiplierTR = multiplierTR + speedTR;
		}
			

	}

	void OnBecameInvisible(){
		Destroy (gameObject);

	}
}
