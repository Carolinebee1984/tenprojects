﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI ;

public class CollisiontoLoseR : MonoBehaviour {
	[SerializeField] Text loseText;


	// Use this for initialization
	void Start () {
		loseText.text = "";
	}

	// Update is called once per frame
	void Update () {

	}

	void OnTriggerEnter2D(Collider2D other ){
		if (other.gameObject.CompareTag ("Red")) {
			Destroy (other.gameObject);
			//display game over
			loseText.text = "GAME OVER!";
			Time.timeScale = 0;
		}

	}

}