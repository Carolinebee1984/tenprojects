﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RacerPM : MonoBehaviour {


	// Use this for initialization

	private bool isPlayerMoving = false;
	[SerializeField] float speed;

	private int count;
	[SerializeField] Text scoreCounter;

	[SerializeField] Text loseText;




	void Start () {
		loseText.text = "";


	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKeyDown (KeyCode.A) && isPlayerMoving == false) 
		{ 
			StartCoroutine (OneMoveTo (-21f));
		}

		if (Input.GetKeyDown (KeyCode.D) && isPlayerMoving == false) 
		{ 
			StartCoroutine (OneMoveTo (-4.5f));
		}


     }

       IEnumerator OneMoveTo (float tarX)
		{
		isPlayerMoving = true;

		float distance = Vector2.Distance (transform.position, new Vector2 (tarX, 0));

		Vector2 tarPos = new Vector2 (tarX, transform.position.y);

			while (distance > 15.9f)
			{
			distance = Vector2.Distance (transform.position, new Vector2 (tarX, 0));

			if (transform.position.x > tarX)
			{ 
				transform.position = Vector2.MoveTowards (new Vector2 (transform.position.x, transform.position.y), tarPos, speed * Time.deltaTime);
			}


			else if (transform.position.x < tarX)
			{
				transform.position = Vector2.MoveTowards (new Vector2 (transform.position.x, transform.position.y), tarPos, speed * Time.deltaTime);
			}

			yield return new WaitForEndOfFrame ();
			}

		transform.position = new Vector3 (tarX, transform.position.y, transform.position.z);
		isPlayerMoving = false;

		}
	void OnTriggerEnter2D (Collider2D other){
		if (other.gameObject.CompareTag ("Green")) {
			Destroy (other.gameObject);
			TwoRacerGM.GM.Score ();
			//y actualiza contador


		} 
		else if (other.gameObject.CompareTag ("Red")) {
			Destroy (this.gameObject);

			//display game over
			loseText.text = "GAME OVER!";
			Time.timeScale = 0;


		}

	}


}


