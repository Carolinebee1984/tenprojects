﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class addsubs : MonoBehaviour {
	public Button suma;
	public Button resta;
	public Text texto;
	int numero;


	void Start (){
		Button plus = suma.GetComponent <Button> ();
		Button minus = resta.GetComponent <Button> ();
		texto = GetComponent<Text> ();
		suma.onClick.AddListener (Addition);
		resta.onClick.AddListener (Substraction);

	
	}
	void Addition (){
		numero ++;
		texto.text = " " + numero;
	}

	void Substraction (){
		numero = numero - 1;
		texto.text = " " + numero;
	}

}

