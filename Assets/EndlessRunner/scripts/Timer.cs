﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Timer : MonoBehaviour {

	[SerializeField] Text timertext; 
	private float startTime;
	[SerializeField] GameObject player;

	// Use this for initialization
	void Start () {
		startTime = Time.time;
	}


	void Update () {
		float t = Time.time - startTime;

		string minutes = ((int)t / 60).ToString ();
		string seconds = (t % 60).ToString ();
		timertext.text = minutes + ":" + seconds; 
	}


}

