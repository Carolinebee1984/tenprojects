﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class RunnerPM : MonoBehaviour {
	
	[SerializeField] Rigidbody rbER;
    [SerializeField] GameObject player;

	[SerializeField] float speed;

	private bool jump;

	public Text gameoverTxt; 

	// Use this for initialization
	void Start () {
		rbER = GetComponent<Rigidbody> ();
		jump = true;
	}
	
	// Update is called once per frame
	void Update ()
	{
		if (gameObject.transform.position.y < -6) {
			Destroy (gameObject);
			gameoverTxt.text = "Game Over";

			Time.timeScale = 0;
		}

	}
		
	private void FixedUpdate (){

		if (rbER.velocity == new Vector3(0,0,0))
		 {
			jump = false;
		 }

		if (rbER.velocity != new Vector3(0,0,0))
		 {
			jump = true;
		 }

//		if (jump) {rbER.AddForce (Vector3.up * speed);
//			RunnerGM.GMER.multiplier = RunnerGM.GMER.multiplier + RunnerGM.GMER.velocidad;
//		}

		if (Input.GetKeyDown (KeyCode.RightArrow) && jump == false) 
		  {
			transform.Translate (new Vector3 (-3f, 0, 0));
		  }

		if (Input.GetKeyDown (KeyCode.LeftArrow) && jump == false)
		  {
			transform.Translate (new Vector3 (3f, 0, 0) );
		  }
			
		if (Input.GetKey (KeyCode.Space) && jump == false)
		    {
			rbER.velocity = new Vector3 (0f,12f,0f);
			//jump = true;
			RunnerGM.GMER.multiplier = RunnerGM.GMER.multiplier + RunnerGM.GMER.velocidad;
		
		    }
	}


	void OnCollisionEnter (Collision other)
	{
		if (other.gameObject.CompareTag ("Enemyer"))
		{
			Destroy (gameObject);

			gameoverTxt.text = "Game Over";

			Time.timeScale = 0;
		}
	
	}

}



	
	