﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Runner_EM : MonoBehaviour {
	private int minRange = 0;
	private int maxRange = 500;
	private int spawn = 50;

	private Transform spawnParent;

	[SerializeField] GameObject enemyER;


	// Use this for initialization
	void Start () {
		spawnParent = GetComponent <Transform> ();
	}
	
	// Update is called once per frame
	void Update () 
	{
		if (spawn == Random.Range (minRange, maxRange)) 
		{
			Instantiate (enemyER, new Vector3 (spawnParent.transform.position.x, 
				spawnParent.transform.position.y, -spawnParent.transform.position.z), Quaternion.identity);
		}
		
	}
}
