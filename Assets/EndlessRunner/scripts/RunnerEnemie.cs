﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RunnerEnemie : MonoBehaviour {

	private Rigidbody enemyrb;


	// Use this for initialization
	void Start () {
		enemyrb = GetComponent <Rigidbody> ();

	}
	
	// Update is called once per frame
	void Update () {
		enemyrb.AddForce (new Vector3 (0,0,-RunnerGM.GMER.velocidad - RunnerGM.GMER.multiplier));

		RunnerGM.GMER.multiplier = RunnerGM.GMER.multiplier + -RunnerGM.GMER.velocidad;


	}

	void OnTriggerEnter(Collider other)
	{
		Destroy (gameObject); 
	
	}

}
