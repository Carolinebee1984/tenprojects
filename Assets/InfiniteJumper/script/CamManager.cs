﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CamManager : MonoBehaviour {

	[SerializeField] GameObject player;



	private Vector3 offset;


	// Use this for initialization
	void Start () {

		offset = transform.position - player.transform.position;



	}

	// Update is called once per frame
	void Update () {

		transform.position = player.transform.position + offset;


	}



}
