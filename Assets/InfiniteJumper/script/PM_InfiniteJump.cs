﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class PM_InfiniteJump : MonoBehaviour {

	public bool gameIsOver;
	[SerializeField] Rigidbody rbIJ;
	public Transform platformTransform;
	[SerializeField] float addForce = 0;

	bool jump = true;

	[SerializeField] Text gameOverText;

	float randomY = 5f;//pa instanciar
	float randomZ = -2.5f;//^

	public GameObject thePlatform;
	[SerializeField] Text scoreText;
	private int scoreCounter;

	// Use this for initialization
	void Start () {
		rbIJ = GetComponent<Rigidbody> ();	

		scoreCounter = -1;

		gameOverText.enabled = false;

		platformTransform = thePlatform.GetComponent<Transform>();
	}
	
	// Update is called once per frame
	void Update () {

		if (gameObject.transform.position.y < platformTransform.transform.position.y) 
		{
			gameIsOver = true;
		}

		if (gameIsOver == true) 
		{
			rbIJ.isKinematic = true;
			gameOverText.enabled = true;

			if (Input.GetKey("r"))
				{
					scoreCounter = -1;
					SceneManager.LoadScene("InfiniteJump");
				}
		}
	}

	private void FixedUpdate () { //este es  para fisica
		
		if(jump){
			if (Input.GetKey (KeyCode.Space)) {
				addForce += 12;
				Invoke ("resetjump", 0.2f);

			}
				
			rbIJ.AddForce (new Vector3 (0, addForce, addForce * 0.1f));
		}
	}


	void resetjump (){
		addForce = 0;
		jump = false;
	
	}


	void OnTriggerEnter(Collider other) {
		
		if (other.gameObject.CompareTag ("Platform")) {
			    
			Instantiate (thePlatform, new Vector3 (0, randomY, randomZ), Quaternion.identity);

			randomY = randomY + 4f;
			randomZ = randomZ + 4f;

		}
	}

	void OnCollisionEnter (Collision other) {
		if (other.gameObject.CompareTag ("Platform")) {
			jump = true;
			scoreCounter = scoreCounter + 1;
			SetScoreText ();
		}
	}

	void SetScoreText(){

		scoreText.text = "Score " + scoreCounter.ToString ();
	}

	void OnTriggerExit (Collider other){
		Destroy (other.gameObject);
	
	}



}

