﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Platform_Generator : MonoBehaviour {
	[SerializeField] GameObject thePlatform;
	//[SerializeField] Transform generationPoint;

	private float gap = -8.82f;

	void Start (){
	   
		//PlatformSpawner (2.0f);
		StartCoroutine (SpawnPlat());
	
	}

	IEnumerator SpawnPlat (){
		yield return new WaitForSeconds (0.5f);
		PlatformSpawner();
	}
		

	void PlatformSpawner (){  //float spawnPoint inside ()
	 

		for (float y = -0.56f; y <= 7.9f; y++) { 

			Instantiate (thePlatform, new Vector3 (0, y, gap), Quaternion.identity);
			gap++;
		
		} 

		Debug.Log ("pls work");

	}
		

	void OnBecameInvisible (){
		Destroy (thePlatform);
	
	}













//	[SerializeField] float distanceBtw;
//
//	private float platformWidth;
//	private float platformHeight;
//
//	// Use this for initialization
//	void Start () {
//		platformWidth = thePlatform.GetComponent<BoxCollider> ().size.z;
//		platformHeight = thePlatform.GetComponent<BoxCollider> ().size.y;
//	}
//
//	// Update is called once per frame
//	void Update () {
//
//		if (transform.position.z < generationPoint.position.z) {
//			transform.position = new Vector3 (transform.position.z + platformWidth + distanceBtw, platformHeight, transform.position.x);
//			Instantiate (thePlatform, transform.position,transform.rotation);
//		  }
	}
