﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class PlayerController : MonoBehaviour {

	public float speed;
	public Text count_text;
	private int counter;
	public Text winText;

	private Rigidbody2D rigbod2d;

	void Start ()
	{
		rigbod2d = GetComponent<Rigidbody2D> ();
		counter = 0;
		SetCounterText ();
		winText.text = "";
	}


	void FixedUpdate()
	{
		   if (Input.GetKey (KeyCode.W) && transform.position.y < 6.2 ) {
			transform.Translate (Vector2.up * Time.deltaTime * speed);
		   }
		    if (Input.GetKey (KeyCode.S) && transform.position.y > -8) {
			transform.Translate (Vector2.down * Time.deltaTime * speed);
		   }
		   if (Input.GetKey (KeyCode.A) && transform.position.x > -12) {
			transform.Translate (Vector2.left * Time.deltaTime * speed);
		   }
		   if (Input.GetKey (KeyCode.D) && transform.position.x < 12) {
			transform.Translate (Vector2.right * Time.deltaTime * speed);
		   }
			 
		
	}

	void OnTriggerEnter2D (Collider2D other)
	{
		if (other.gameObject.CompareTag ("Picks")){
			Destroy (other.gameObject);
			counter = counter +1;
			SetCounterText ();
		}
			
	
	}
	void SetCounterText ()
	{
		count_text.text = "Your Pepperonis: " + counter.ToString ();
		if (counter >= 10) 
		{
			winText.text = "You win!";
			Time.timeScale = 0;
		}

	}


}

