﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour {

	[SerializeField] float speed;
	[SerializeField] private Transform leftPaddle;
	[SerializeField] private Transform rightPaddle;
	//[SerializeField] Score score;

	// Use this for initialization
	void Start () {

	}

	// Update is called once per frame
	void Update () {

		if (Input.GetKey (KeyCode.W) && leftPaddle.position.y < 2.4f) {
			leftPaddle.Translate (Vector2.up * Time.deltaTime * speed);
		}

		if (Input.GetKey (KeyCode.S) && leftPaddle.position.y > -3.2f) {
			leftPaddle.Translate (Vector2.down * Time.deltaTime * speed);
		}

		if (Input.GetKey (KeyCode.UpArrow) && rightPaddle.position.y < 2.4f) {
			rightPaddle.Translate (Vector2.up * Time.deltaTime * speed);
		}

		if (Input.GetKey (KeyCode.DownArrow) && rightPaddle.position.y > -3.2f) {
			rightPaddle.Translate (Vector2.down * Time.deltaTime * speed);
		}
	}


}

