﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class Ball : MonoBehaviour {
	private Rigidbody2D rb;


	// Use this for initialization
	void Start (){

		StartCoroutine (Pause ());

		//velocidad inicial
		rb= GetComponent<Rigidbody2D>();
	}

	void Update () {
		 
		if (transform.position.x < -7f) {

			transform.position = Vector3.zero;
			rb.velocity = Vector3.zero;

			//Give player 2 a point
			Score.instance.GiveRightPaddleAPoint();


			StartCoroutine (Pause ());
		}


		if (transform.position.x > 7f) {
			
			transform.position = Vector3.zero;
			rb.velocity = Vector3.zero;

			Score.instance.GiveLeftPaddleAPoint();

			StartCoroutine (Pause());
		}

	}
		

	IEnumerator Pause (){

		yield return new WaitForSeconds (2.5f);
		LaunchBall ();

	}
	void LaunchBall(){
		//direccion random en x 
		int xDirection = Random.Range(0,2);

		//direccion random en y
		int yDirection = Random.Range(0,2);

		Vector2 launchDirection = new Vector2 ();

		if (xDirection == 0){
			launchDirection.x = -8f;
		}

		if (xDirection == 1){
			launchDirection.x = 8f;

		}

		if (yDirection == 0){
			launchDirection.y = -8f;
		}

		if (yDirection == 1){
			launchDirection.y = 8f;

		}

		rb.velocity = launchDirection;


	}



}


		



