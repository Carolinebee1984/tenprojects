﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Score : MonoBehaviour {
	
	[SerializeField] Text LeftScoreText;
	[SerializeField] Text RightScoreText;

	public static Score instance;

	public int leftScore;
	public int rightScore;

	void Start (){
	  
		instance = this;
		leftScore = rightScore = 0;
	
	}


	void Update (){
	
	
	
	}

	public void GiveLeftPaddleAPoint (){
      
		leftScore += 1;

		LeftScoreText.text = leftScore.ToString ();
	
	}

	public void GiveRightPaddleAPoint (){

		rightScore += 1;

		RightScoreText.text = rightScore.ToString ();
	}

	}

